const textsToChange = document.querySelectorAll("[data-section]");

var selectLanguage = async function()
{
    let select = document.getElementById("selectLanguage");
    let value = select.value;
    const request = 
        await fetch(`../language/${value}.json`)
        .then((data)=> {return data.json()})
        .then(
            (res) => 
            {
                console.log(res)
                console.log( textsToChange)
                for (texts of textsToChange)
                {
                    console.log(texts)
                    let section = texts.dataset.section;
                    let value = texts.dataset.value;
                    texts.innerHTML = res[section][value];
                }
            },
            (err) =>
            {
                console.log("Error:",err)
            }
        );
};

const loadMenu  = () => {
    let check = document.getElementById("checkboxMenu").checked;
    let sidebar = document.getElementById("sidebar-menu");
    let backdrop = document.getElementById("backdrop");
    let select = document.getElementById("select-wrapper");

    backdrop.classList.add("open");
    sidebar.classList.remove("menu-off");
    select.classList.add("topbarOpen");

    if (!check)
    {
        select.classList.remove("topbarOpen");
        sidebar.classList.add("menu-off");
        backdrop.classList.remove("open");
    }
};

document.getElementById("checkboxMenu").addEventListener("click", loadMenu);
